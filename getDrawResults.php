<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 24.09.2018
 * Time: 22:29
 */

if (!array_key_exists('argc', $_SERVER)) {
 	die('Skrypt może zostać uruchomiony tylko z linii komend!');
}

require_once 'vendor/autoload.php';
require_once("app/Autoloader.php");


use Cfg\Cfg;
use File\File;
use File\Formatter\Json;
use Parser\Parser;

try{
    $cfg = new Cfg('sources.json');
    $file = new File("drawResults.json", new Json());
    foreach($cfg->getCfgArray() as $config){
        $parser = new Parser(new $config['driver']);
        $drawResults = $parser->getData();
        $file->addArray($drawResults);
        empty($parser);
    }
    $file->save();
}catch(Exception $e){
    echo $e->getMessage();
}catch(Error $e){
    echo $e->getMessage();
}

