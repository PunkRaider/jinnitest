<?php
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 23.09.2018
 * Time: 23:31
 */
declare(strict_types=1);
namespace Cfg;


use Exception;

class Cfg
{
    private $configDir = __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."cfg".DIRECTORY_SEPARATOR;
    private $cfg = array();
    /**
     * Cfg constructor.
     * @param string $configFileName
     * @throws Exception
     */
    public function __construct(string $configFileName)
    {
        $filePath = $this->configDir.$configFileName;
        if(!file_exists($filePath)) throw new Exception("Plik konfiguracyjny nie istnieje: ".$filePath);

        $fileArray = file($filePath);

        $cfgFileContent = implode("", $fileArray);

        $cfg = json_decode($cfgFileContent, true);
        if(!is_array($cfg)) throw new Exception("Błąd ładowania pliku konfiguracyjnego: ".$filePath);

        $this->cfg = $cfg;
    }

    public function getCfgArray() : array
    {
        return $this->cfg;
    }
}