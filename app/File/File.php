<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 24.09.2018
 * Time: 21:47
 */

namespace File;


use File\Formatter\FormatterInterface;

class File
{
    private $formatter;
    private $fileContent = array();
    private $filePath = "";


    /**
     * File constructor.
     * @param string $filePath
     * @param FormatterInterface $formatter
     */
    public function __construct(string $filePath, FormatterInterface $formatter)
    {
        $this->filePath = $filePath;
        $this->formatter = $formatter;
    }

    /**
     * @param string $content
     */
    public function addLine(string $content) : void
    {
        $this->fileContent[] = $content;
    }

    /**
     * @param array $content
     */
    public function addArray(array $content) : void
    {
        $this->fileContent = array_merge($this->fileContent, $content);
    }

    /**
     * @return bool
     */
    public function save() : bool
    {
        $fileContent = $this->formatter->format($this->fileContent);
        $res = file_put_contents($this->filePath, $fileContent);

        return $res > 0 ? true : false;
    }


}
