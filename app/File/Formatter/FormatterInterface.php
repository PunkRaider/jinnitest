<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 24.09.2018
 * Time: 21:48
 */

namespace File\Formatter;


interface FormatterInterface
{
    /**
     * @param array $data
     * @return string
     */
    public function format(array $data) : string;

    /**
     * @param string $data
     * @return array
     */
    public function readData(string $data) : array;
}