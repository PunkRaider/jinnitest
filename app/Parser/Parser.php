<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 24.09.2018
 * Time: 21:08
 */

namespace Parser;


use Curl\Curl;
use DateTime;
use ErrorException;
use Parser\Driver\DrvInterface;

class Parser
{
    private $driver;

    /**
     * Parser constructor.
     * @param DrvInterface $drv
     */
    public function __construct(DrvInterface $drv)
    {
        $this->driver = $drv;
    }

    /**
     * @return array
     * @throws ErrorException
     */
    public function getData() : array
    {
        $source = $this->getSource();
        $parsedData = $this->driver->parse($source);
        $out = $this->normalizeDateFields($parsedData);
        return $out;

    }

    /**
     * @return string
     * @throws ErrorException
     */
    private function getSource() : string
    {
        $curl = new Curl();
        $curl->get($this->driver->getUrl());
        return $curl->response;
    }


    /**
     * @param array $sourceData
     * @return array
     */
    private function normalizeDateFields(array $sourceData) : array
    {
        $out = array();
        foreach($sourceData as $draw){
            $date = DateTime::createFromFormat($this->driver->getDateFormat(), $draw['data_losowania']);
            $outTemp = array();
            foreach($draw as $key => $field){
                if('data_losowania' == $key) $outTemp[$key] = $date->format("d-m-Y");
                else $outTemp[$key] = $field;
            }
            $out[] = $outTemp;
        }
        return $out;
    }
}