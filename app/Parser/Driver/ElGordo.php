<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 23.09.2018
 * Time: 23:08
 */

namespace Parser\Driver;


class ElGordo implements DrvInterface
{
    private $url = "https://www.elgordo.com/results/euromillonariaen.asp";
    private $dateFormat = "d-F-Y";
    private $returnData;

    /**
     * @param string $pageContent
     * @return array
     * @throws \Exception
     */
    public function parse(string $pageContent): array
    {
        preg_match_all('/<span class="int-num">([0-9]*)<\/span>/m', $pageContent, $rawRow);
        preg_match_all('/<div class="body_game ee">[\s]*<div style="padding:10px 0;" class="c">[^,]*,&nbsp;([0-9]*)&nbsp;(.*)&nbsp;([0-9]*)<\/div>/m', $pageContent, $matches2);
        $date = $matches2[1][0].'-'.$matches2[2][0].'-'.$matches2[3][0];
        if("" == $rawRow[1][0]) throw new \Exception("Błąd przygotowywaniu danych dla wyników ElGordo - niezgodna struktura źródła!");

        $this->returnData[] = $this->prepareOutputRow($rawRow[1], $date);
        return $this->returnData;
    }

    /**
     * @return string
     */
    public function getDateFormat(): string
    {
        return $this->dateFormat;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param array $rawRow
     * @param $drawDate
     * @return array
     */
    protected function prepareOutputRow(array $rawRow, $drawDate) : array
    {
        $retData = [
            "typ_losowania"     => "El Gordo",
            "nr_losowania"      => "",
            "data_losowania"    => $drawDate
        ];
        foreach($rawRow as $key => $elem){
            $retData['wyniki'][] = $elem;

        }

        return $retData;
    }
}