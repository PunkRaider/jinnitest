<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 23.09.2018
 * Time: 21:20
 */

namespace Parser\Driver;


abstract class LottoCommon implements DrvInterface
{
    protected $url = '';
    protected $dateFormat = '';
    protected $returnData = array();

    /**
     * @param string $sourceString
     * @return array
     * @throws \Exception
     */
    protected function prepareData(string $sourceString) : array
    {
        $content = str_ireplace('<tr class="wynik"', '^', $sourceString );
        preg_match_all('/\^ style="line-height: [0-9]*px">[^\^]*/m', $content, $rowsSections);
        if("" == $rowsSections[0][0]) $this->pushStructureError();
        return $rowsSections[0];
    }

    /**
     * @param string $sourceString
     * @return array
     * @throws \Exception
     */
    protected function parseSevenNumbers(string $sourceString) : array
    {
        preg_match_all('/<td>([0-9]*)<\/td><td>([0-9\-]*), [^<]*<\/td>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*/m', $sourceString, $rawData);
        if("" == $rawData[0][0]) $this->pushStructureError();
        return $rawData;
    }

    /**
     * @param string $sourceString
     * @return array
     * @throws \Exception
     */
    protected function parseSixNumbers(string $sourceString) : array
    {
        preg_match_all('/<td>([0-9]*)<\/td><td>([0-9\-]*), [^<]*<\/td>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*<span>([0-9]*)<\/span>.*/m', $sourceString, $rawData);
        if("" == $rawData[0][0]) $this->pushStructureError();
        return $rawData;
    }

    /**
     * @param string $sourceString
     * @return string
     * @throws \Exception
     */
    protected function getDrawType(string $sourceString) : string
    {
        preg_match('/alt="([a-zA-Z\s]*)"/', $sourceString, $type);
        if("" == $type[1]) $this->pushStructureError();
        return $type[1];
    }

    /**
     * @param array $rawRow
     * @param string $drawType
     * @return array
     */
    protected function prepareOutputRow(array $rawRow, string $drawType) : array
    {
        $retData = ["typ_losowania" => $drawType];
        foreach($rawRow as $key => $elem){
            switch($key) {
                case 1:
                    $retData['nr_losowania'] = $elem[0];
                    break;
                case 2:
                    $retData['data_losowania'] = $elem[0];
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    $retData['wyniki'][] = $elem[0];
                    break;
            }
        }
        return $retData;
    }

    /**
     * @throws \Exception
     */
    protected function pushStructureError() : void
    {
        throw new \Exception("Błąd przygotowywaniu danych dla wyników Lotto - niezgodna struktura źródła!");
    }

    /**
     * @return string
     */
    public function getDateFormat(): string
    {
        return $this->dateFormat;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}