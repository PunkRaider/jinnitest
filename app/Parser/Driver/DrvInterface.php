<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 23.09.2018
 * Time: 21:14
 */

namespace Parser\Driver;


interface DrvInterface
{
    public function parse(string $pageContent) : array;
    public function getDateFormat() : string;
    public function getUrl() : string;
}