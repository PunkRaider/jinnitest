<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 23.09.2018
 * Time: 21:20
 */

namespace Parser\Driver;


class Lotto extends LottoCommon
{
    protected $url = 'https://www.lotto.pl/lotto/wyniki-i-wygrane';
    protected $dateFormat = 'd-m-y';

    /**
     * @param string $pageContent
     * @return array
     * @throws \Exception
     */
    public function parse(string $pageContent): array
    {
        $preparedArray = $this->prepareData($pageContent);
        foreach($preparedArray as $row){
            $typ = $this->getDrawType($row);
            $rawRow = array();
            if('Super Szansa' == $typ){
                $rawRow = $this->parseSevenNumbers($row);
            }elseif('Lotto' == $typ || 'Lotto Plus' == $typ){
                $rawRow = $this->parseSixNumbers($row);
            }
            $this->returnData[] = $this->prepareOutputRow($rawRow, $typ);
        }

        return $this->returnData;

    }

}