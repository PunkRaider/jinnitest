<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: misiek
 * Date: 23.09.2018
 * Time: 22:45
 */

namespace Parser\Driver;


class EuroJackpot extends LottoCommon
{
    protected $url = "https://www.lotto.pl/eurojackpot/wyniki-i-wygrane";
    protected $dateFormat = 'd-m-y';

    /**
     * @param string $pageContent
     * @return array
     * @throws \Exception
     */
    public function parse(string $pageContent): array
    {
        $preparedArray = $this->prepareData($pageContent);
        foreach($preparedArray as $row){
            $rawRow = $this->parseSevenNumbers($row);
            $this->returnData[] = $this->prepareOutputRow($rawRow, 'Euro Jackpot');
        }

        return $this->returnData;
    }
}