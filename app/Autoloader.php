<?php
declare(strict_types=1);
/**
 * Class Autoloader
 * @author Michal Ryczanczyk <michal@ryczanczyk.net>
 */
class Autoloader
{
    /**
     * @return void
     */
    public static function register() : void
    {
        spl_autoload_register(function ($class) {
            $file = 'app'.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
            if (file_exists($file)) {
                require $file;
                return true;
            }
            return false;
        });
    }
}
Autoloader::register();